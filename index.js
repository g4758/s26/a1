//No. 1 What directive is used by Node.js in loading the modules it needs?
// require()

//No. 2 What Node.js module contains a method for server creation?
// Http Module

// No. 3 What is the method of the http object responsible for creating a server using Node.js?
//.createServer() method

// No. 4 What method of the response object allows us to set status codes and content types?
// writeHead

// No. 5 Where will console.log() output its contents when run in Node.js?
//

// No. 6 What property of the request object contains the address' endpoint?
//end()

const HTTP = require(`http`);

HTTP.createServer((request,response) => {

if(request.url ==="/login"){
response.writeHead(200, {"Content-Type": "text/plain"});
response.write("Bonjour! welcome to login page");
response.end() }

else{
    response.writeHead(400, {"Content-Type": "text/plain"});
    response.write("The page you are looking for is similar to John Cena. It cannot be found...");
    response.end()
}

}).listen(3000);

// console.log("I'am Terminal I speak for the trees");

